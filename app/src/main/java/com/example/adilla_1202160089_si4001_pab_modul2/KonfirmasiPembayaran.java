package com.example.adilla_1202160089_si4001_pab_modul2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class KonfirmasiPembayaran extends AppCompatActivity {
    private TextView tujuan_ed, berangkat_ed, pulang_ed, jumlahTiket_ed, biaya_ed;
    private int tujuan, jam, menit, biaya, jmlTiket;
    private String tanggalpulang, tanggal = "tanggal", nama_tujuan;
    private String[] tujuansemua = {"Jakarta", "Cirebon", "Bekasi"};
    private boolean switch1 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        Bundle sum = intent.getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_pembayaran);
        Log.d("Summary - Bundle", "Bundle received!");
        if (sum != null) {
            tujuan = sum.getInt("tujuan", 0);
            jmlTiket = sum.getInt("jmlTiket", 0);
            jam = sum.getInt("jam", 0);
            menit = sum.getInt("menit", 0);
            biaya = sum.getInt("biaya", 0);
            switch1 = sum.getBoolean("switche", false);
            if (switch1) {
                tanggalpulang = sum.getString("tanggalpulang", "null");
            }
            tanggal = sum.getString("tanggal", "null");
            Log.d("Summary - Bundle", "Bundle opened!");
        }

        //untuk mengatur tujuan
        tujuan_ed = findViewById(R.id.tujuan_ed);
        switch (tujuan) {
            case 0:
                nama_tujuan = tujuansemua[0];
                tujuan_ed.setText(nama_tujuan);
                break;
            case 1:
                nama_tujuan = tujuansemua[1];
                tujuan_ed.setText(nama_tujuan);
                break;
            case 2:
                nama_tujuan = tujuansemua[2];
                tujuan_ed.setText(nama_tujuan);
                break;
        }

        //untuk mengatur tanggal
        berangkat_ed = findViewById(R.id.berangkat_ed);
        berangkat_ed.setText(tanggal);
        if (switch1) {
            pulang_ed = findViewById(R.id.pulang_ed);
            pulang_ed.setText(tanggalpulang);
        } else {
            pulang_ed = findViewById(R.id.pulang_ed);
            TextView txtpulang = findViewById(R.id.tgl_kepulangan);
            pulang_ed.setVisibility(View.GONE);
            txtpulang.setVisibility(View.GONE);
        }

        //untuk mengatur jumlah tiket
        jumlahTiket_ed = findViewById(R.id.jmltiket_ed);
        jumlahTiket_ed.setText(Integer.toString(jmlTiket));

        //untuk mengatur total biaya
        biaya_ed = findViewById(R.id.biaya_ed);
        biaya_ed.setText(getString(R.string.total_txt) + Integer.toString(biaya));
        Log.d("Summary-onCreate", "");
    }

    public void konfirm(View view) {
        Log.d("Summary-konfirm", "");
        Toast.makeText(this, getString(R.string.toast_thanks), Toast.LENGTH_LONG)
                .show();
        Intent pulangkanlah = new Intent();
        pulangkanlah.putExtra("newsaldo", Integer.toString(biaya));
        setResult(Activity.RESULT_OK, pulangkanlah);
        Log.d("Summary-konfirm", "Finish!");
        finish();
    }
}
